<?php include_once 'admin_includes/main_header.php'; ?>	
<?php  
if (!isset($_POST['submit']))  {
            echo "";
} else  {
    //print_r($_POST);exit;
    //Save data into database
    $product_code = rand(1234,9876);
    $cat_id = $_POST['cat_id'];
    $subcat_id = $_POST['subcat_id'];
    $product_name = $_POST['product_name'];
    $product_description = $_POST['product_description'];
    $product_info = $_POST['product_info'];
    $product_price = $_POST['product_price'];
    $fileToUpload = $_FILES["fileToUpload"]["name"];
    $availability_id = $_POST['availability_id'];
    $created_at = date("Y-m-d h:i:s");
    $created_by = $_SESSION['admin_user_id'];

    if($fileToUpload!='') {

        $target_dir = "../uploads/product_images/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            $sql = "INSERT INTO products (`product_code`,`cat_id`,`subcat_id`,`product_name`,`product_description`,`product_info`,`product_price`,`product_image`,`availability_id`,`created_by`,`created_at`) VALUES ('$product_code','$cat_id','$subcat_id','$product_name','$product_description','$product_info','$product_price','$fileToUpload','$availability_id','$created_by','$created_at')";
            $result = $conn->query($sql);
            $new_product = array_filter($_POST['product_type_id']);
            $product_type_id = count($new_product);
            for ($i=0; $i < $product_type_id ; $i++) { 
                $product_type_code = rand(9658,2365);
                $product_type_id1 = $_POST['product_type_id'][$i];
                $availability_id1 = $_POST['availability_id1'][$i];
                $required_id = $_POST['required_id'][$i];
                $choose_note = $_POST['choose_note'][$i];
                $sql1 = "INSERT INTO product_details ( `product_code`,`product_type_code`,`ptoduct_type_id`,`availability_id`,`required_id`,`choose_note`) VALUES ('$product_code','$product_type_code','$product_type_id1','$availability_id1','$required_id','$choose_note')";
                $result1 = $conn->query($sql1);
                $type_name_id = count($_POST['type_name_id']);

                for ($j=0; $j < $type_name_id ; $j++) { 
                    $type_name_id = $_POST['type_name_id'][$j];
                    $type_code = getSingleColumnName($type_name_id,'id','product_type_code','product_type_details');
                    if($_POST['product_type_id'][$i] == $type_code) {
                        $sql2 = "INSERT INTO product_name_details ( `product_code`,`product_type_code`,`type_name_id`) VALUES ('$product_code','$product_type_code','$type_name_id')";
                        $result2 = $conn->query($sql2);
                    }
                }
            }
            if($result == 1 && $result1 == 1 ){
                echo "<script type='text/javascript'>window.location='products.php?msg=success'</script>";
            } else {
                echo "<script type='text/javascript'>window.location='products.php?msg=fail'</script>";
            }
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }      
    
}
?>
      <div class="site-content">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="m-y-0">Products</h3>
          </div>
          <div class="panel-body">
            <div class="row">
              <?php $getCategories = getAllDataWithStatus('categories','0');?>
              <?php $getProductTypes = getAllDataWithStatus('product_types','0');?>
                <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                    <form data-toggle="validator" method="post" enctype="multipart/form-data">
                    
                    <div class="form-group">
                        <label for="form-control-3" class="control-label">Choose your Category</label>
                        <select id="form-control-3" name="cat_id" class="custom-select cat_id" data-error="This field is required." required >
                        <option value="">Select Category</option>
                        <?php while($row = $getCategories->fetch_assoc()) {  ?>
                            <option value="<?php echo $row['id']; ?>"><?php echo $row['category_name']; ?></option>
                        <?php } ?>
                    </select>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="form-control-3" class="control-label">Choose your Sub Category</label>
                        <select id="form-control-3" name="subcat_id" class="custom-select sub_cat_id" data-error="This field is required." required >
                        <option value="">Select Sub Category</option>
                    </select>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="form-control-2" class="control-label">Product Name</label>
                        <input type="text" class="form-control" id="product_name" name="product_name" placeholder="Product Name" data-error="Please Enter Product name." required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="form-control-2" class="control-label">Product Price</label>
                        <input type="number" class="form-control" id="product_price" name="product_price" placeholder="Product Price" data-error="Please Enter Product Price." required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="input_fields_container">
                        <div class="form-group">
                            <label for="form-control-3" class="control-label">Choose Product Type</label>
                            <select id="form-control-3" name="product_type_id[]" class="custom-select product_type_id" data-error="This field is required." required>
                                <option value="">Select Product Type</option>
                                <?php while($row = $getProductTypes->fetch_assoc()) {  ?>
                                    <option value="<?php echo $row['product_type_code']; ?>"><?php echo $row['product_type']; ?></option>
                                <?php } ?>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <label for="form-control-3" class="control-label">Choose Type Name</label>
                            <select id="type_name_id" multiple="multiple" name="type_name_id[]" class="custom-select type_name_id" data-error="This field is required." required>
                                <option value="">Select Type Name</option>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <label for="form-control-3" class="control-label">Avalability</label>
                            <select id="form-control-3" name="availability_id1[]" class="custom-select" data-error="This field is required." required>
                                    <option value="">Avalability</option>
                                    <option value="0">In Stock</option>
                                    <option value="1">Sold Out</option>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <label for="form-control-3" class="control-label">Required</label>
                            <select id="form-control-3" name="required_id[]" class="custom-select" data-error="This field is required." required>
                                <option value="">Avalability</option>
                                    <option value="0">Yes</option>
                                    <option value="1">No</option>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <label for="form-control-2" class="control-label">Note</label>
                            <input type="text" class="form-control" id="choose_note[]" name="choose_note[]" placeholder="Note(Choose 1)" data-error="Please Enter NOte." required>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <span><button type="button" class="btn btn-success add_more_button">Add More</button></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="form-control-2" class="control-label">Product Info</label>
                        <textarea name="product_info" class="form-control" id="product_info" placeholder="Special Instructions" data-error="This field is required." required></textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="form-control-2" class="control-label">Description</label>
                        <textarea name="product_description" class="form-control" id="description" placeholder="Description" data-error="This field is required." required></textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="form-control-4" class="control-label">Image</label>
                        <img id="output" height="100" width="100"/>
                        <label class="btn btn-default file-upload-btn">
                        Choose file...
                            <input id="form-control-22" class="file-upload-input" type="file" accept="image/*" name="fileToUpload" id="fileToUpload"  onchange="loadFile(event)"  multiple="multiple" required >
                        </label>
                    </div>
                    <div class="form-group">
                        <label for="form-control-3" class="control-label">Avalability</label>
                        <select id="form-control-3" name="availability_id" class="custom-select" data-error="This field is required." required>
                            <option value="">Avalability</option>
                                <option value="0">In Stock</option>
                                <option value="1">Sold Out</option>
                        </select>
                        <div class="help-block with-errors"></div>
                    </div>
                    <button type="submit" name="submit" value="Submit"  class="btn btn-primary btn-block">Submit</button>
                    </form>
                </div>
            </div>
            <hr>
          </div>
        </div>
      </div>
      <?php include_once 'admin_includes/footer.php'; ?>
   <script src="js/tables-datatables.min.js"></script>
   <script src="js/multi_image_upload.js"></script>
   <link rel="stylesheet" type="text/css" href="css/multi_image_upload.css">
   
   <!-- Below script for ck editor -->
<script src="//cdn.ckeditor.com/4.7.0/full/ckeditor.js"></script>
<script>
    /*CKEDITOR.replace( 'product_info' );*/
</script>

<?php
    $sql1 = "SELECT * FROM product_weights where status = '0'";
    $result1 = $conn->query($sql1);                                    
?>

<?php while($row = $result1->fetch_assoc()) { 
   $choices1[] = $row['id'];
   $choices_names[] = $row['weight_type'];
} ?>


<script>
    $(document).ready(function() {
    var max_fields_limit      = 10; //set limit for maximum input fields
    var x = 1; //initialize counter for text box
    $("body").on("click",".add_more_button", function (e) { //click event on add more fields button having class add_more_button
        e.preventDefault();
        if(x < max_fields_limit){ //check conditions
            x++; //counter increment
            $('.input_fields_container').append($("<div>").load("add_product_type_details.php?count="+x)); //add input field
        }
    });  
    $('.input_fields_container').on("click",".remove_field", function(e){ //user click on remove text links
        e.preventDefault(); $(this).parent().parent().parent().remove(); x--;
    })
});
</script>
<script type="text/javascript">

//Script allowed only numeric value
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

$("body").on("change",".product_type_id", function (e) {
    var id =$(this).val();
    $.ajax({
    type: "POST",
    url: "get_type_names.php",
    data:'type_id='+id,
    success: function(data){
        $("#type_name_id").html(data);
    }
    });
});
$("body").on("change",".product_type_id1", function (e) {
    var id =$(this).val();
    var typeId = $(this).attr("type-id");
    $.ajax({
    type: "POST",
    url: "get_type_names.php",
    data:'type_id='+id,
    success: function(data){
        $(".type_name_id"+typeId).html(data);
    }
    });
});
</script>
<script>
$("body").on("change",".cat_id", function (e) {
    var id =$(this).val();
    $.ajax({
    type: "POST",
    url: "get_sub_categories.php",
    data:'category_id='+id,
    success: function(data){
        $(".sub_cat_id").html(data);
    }
    });
});
</script>
<script src="//cdn.ckeditor.com/4.7.0/full/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'description' ); 
</script>
<style type="text/css">
    .cke_top, .cke_contents, .cke_bottom {
        border: 1px solid #333;
    }
</style>