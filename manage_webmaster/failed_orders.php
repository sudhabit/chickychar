<?php include_once 'admin_includes/main_header.php'; ?>

      <style>
        .table1 {
          display: table;
          border: 1px solid #808080;
          text-align: center;
          border-top-left-radius: 3px;
          border-top-right-radius: 3px;
          padding: 0px;
          margin-bottom: 0;
        }
        .table1-row {
            display: table-row;
        }
        .table1-cell {
          display: table-cell;
          border-bottom: 1px solid #b2b2b2;
          padding: 6px;
        }
        .table1-footer .table1-cell{
          border-bottom: 0;
          padding-top:4px;
        }
        .table1-header {
          font-weight: bold;
          background-color: #d8d8d8;
        }
      </style>

      <div class="site-content">
        <div class="panel panel-default panel-table">
          <div class="panel-heading">
            <!-- <a href="add_orders.php" style="float:right">Add Order</a> -->
            <h3 class="m-t-0 m-b-5">Failed Orders</h3> 
            <?php $sql ="SELECT * from orders WHERE payment_status=3 GROUP BY order_id  ORDER BY order_status DESC";
              $res = $conn->query($sql);
              $i=1; 
            ?>
          </div>
          <div class="panel-body">
            <div class="table-responsive" style="overflow-x: hidden;">
            
         
          <!-- <div class="clear_fix"></div> -->
          
              <table class="table table-striped table-bordered dataTable" id="table-1">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Name</th>
                    <th>Order Id</th>
                    <th>Mobile No</th>
                    <th>Order Date</th>
                    <th>Payment Mode</th>
                    <th>Order Status</th>
                    <th>Payment Status</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  <?php while($res1 = $res->fetch_assoc()) { ?>
                  <tr>
                    <td><?php echo $i;?></td>
                    <td><?php echo $res1['first_name'];?></td>
                    <td><?php echo $res1['order_id'];?></td>
                    <td><?php echo $res1['mobile'];?></td>
                    <td><?php echo $res1['order_date'];?></td> 
                    <th><?php if($res1['payment_type'] == '1' ) { echo "COD"; } else { echo "Online"; } ?></th>
                    <td>Order Failed</td>
                    <td>Payment Failed</td>
                    <td><a target="_blank" href="total_order_details.php?order_id=<?php echo $res1['order_id']; ?>"><i class="zmdi zmdi-local-printshop"  class=""></i></a></td>
                  </tr>
                  <?php  $i++; } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
   <?php include_once 'admin_includes/footer.php'; ?>
   <script src="js/tables-datatables.min.js"></script>