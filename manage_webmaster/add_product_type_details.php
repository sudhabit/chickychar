<?php 
include_once('admin_includes/config.php');
include_once('admin_includes/common_functions.php');
$getProductTypes = getAllDataWithStatus('product_types','0');
$count = $_GET['count'];
?>
<div class="form-group">
    <label for="form-control-3" class="control-label">Choose Product Type</label>
    <select id="form-control-3" name="product_type_id[]" type-id="<?php echo $count; ?>" class="custom-select product_type_id1" data-error="This field is required." required>
    <option value="">Select Product Type</option>
    <?php while($row = $getProductTypes->fetch_assoc()) {  ?>
        <option value="<?php echo $row['product_type_code']; ?>"><?php echo $row['product_type']; ?></option>
    <?php } ?>
</select>
    <div class="help-block with-errors"></div>
</div>
<div class="form-group">
    <label for="form-control-3" class="control-label">Choose Type Name</label>
    <select id="type_name_id" multiple="multiple" name="type_name_id[]" class="custom-select type_name_id<?php echo $count; ?>" data-error="This field is required." required>
        <option value="">Select Type Name</option>
    </select>
    <div class="help-block with-errors"></div>
</div>
<div class="form-group">
    <label for="form-control-3" class="control-label">Avalability</label>
    <select id="form-control-3" name="availability_id1[]" class="custom-select" data-error="This field is required." required>
            <option value="">Avalability</option>
            <option value="0">In Stock</option>
            <option value="1">Sold Out</option>
    </select>
    <div class="help-block with-errors"></div>
</div>
<div class="form-group">
    <label for="form-control-3" class="control-label">Required</label>
    <select id="form-control-3" name="required_id[]" class="custom-select" data-error="This field is required." required>
        <option value="">Avalability</option>
            <option value="0">Yes</option>
            <option value="1">No</option>
    </select>
    <div class="help-block with-errors"></div>
</div>
<div class="form-group">
    <label for="form-control-2" class="control-label">Note</label>
    <input type="text" class="form-control" id="choose" name="choose_note[]" placeholder="Note(Choose 1)" data-error="Please Enter NOte." required>
    <div class="help-block with-errors"></div>
</div>
<div class="form-group">
    <span><a href="#" class="remove_field btn btn-warning" style="margin-left:15px">Remove</a></span>
</div>