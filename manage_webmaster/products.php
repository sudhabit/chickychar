<?php include_once 'admin_includes/main_header.php'; ?>
<?php 
$getData ="SELECT * FROM products ORDER BY availability_id,status, id DESC";
$getProductsData = $conn->query($getData);
$i=1; 
if(isset($_POST['submit'])) { 
  $sql = "UPDATE `products` SET status=0";
  $conn->query($sql);  
  Header('Location: '.$_SERVER['PHP_SELF']);
}
if(isset($_POST['submit1'])) { 
  $sql1 = "UPDATE `products` SET status=1";
  $conn->query($sql1);  
  Header('Location: '.$_SERVER['PHP_SELF']);
} 
if(isset($_POST['submit3'])) { 
  $sql = "UPDATE `products` SET availability_id=0";
     $conn->query($sql);  
     Header('Location: '.$_SERVER['PHP_SELF']);
}
if(isset($_POST['submit4'])) { 
  $sql1 = "UPDATE `products` SET availability_id=1";
  $conn->query($sql1);  
  Header('Location: '.$_SERVER['PHP_SELF']);
}
if(isset($_POST['submit5'])) { 
  $sql = "UPDATE `products` SET active_pop_up=0";
     $conn->query($sql);  
     Header('Location: '.$_SERVER['PHP_SELF']);
}
if(isset($_POST['submit6'])) { 
  $sql1 = "UPDATE `products` SET active_pop_up=1";
  $conn->query($sql1);  
  Header('Location: '.$_SERVER['PHP_SELF']);
}
?>
<div class="site-content">
  <div class="panel panel-default panel-table">
    <div class="panel-heading">
      <a href="add_products.php" style="float:right">Add Products</a>
      <h3 class="m-t-0 m-b-5">Products</h3>
    </div>
    <div class="panel-body">
    <div class="table-responsive">
      <!-- <form method="post" action="">
        <button type="submit" name="submit">All Active</button>
      </form>
      <form method="post" action="">
        <button type="submit" name="submit1">All InActive</button>
      </form> 
    </div>
    <div class="form-group col-md-4">
      <form method="post" action="">
        <button type="submit" name="submit3">All In Stock</button>
      </form>
      <form method="post" action="">
        <button type="submit" name="submit4">All Out of Stock</button>
      </form> 
    </div>
    <div class="form-group col-md-4">
      <form method="post" action="">
        <button type="submit" name="submit5">All Active pop Up</button>
      </form>
      <form method="post" action="">
        <button type="submit" name="submit6">All InActive Pop Up</button>
      </form>  
    </div> -->
    <div class="clear_fix"></div>
      <table class="table table-striped table-bordered dataTable" id="table-1">
        <thead>
          <tr>
            <th>S.No</th>
            <th>Product Name</th>
            <th>Category Name</th>
            <th>Sub Category Name</th>
            <th>Availability</th>
            <th>Status</th>
            <th>Actions</th>
            
          </tr>
        </thead>
        <tbody>
          <?php while ($row = $getProductsData->fetch_assoc()) { ?>
          <?php $category = getSingleColumnName($row['cat_id'],'id','category_name','categories'); ?>
          <?php $sub_category = getSingleColumnName($row['subcat_id'],'id','sub_category_name','sub_categories'); ?>
          <tr>
            <td><?php echo $i;?></td>
            <td><?php echo $row['product_name'];?></td>
            <td><?php echo $category;?></td>
            <td><?php echo $sub_category;?></td>
            <td><?php if ($row['availability_id']==0) { echo "<span class='label label-outline-success check_active1 open_cursor' data-incId=".$row['id']." data-status=".$row['availability_id']." data-tbname='products'>In Stock</span>" ;} else { echo "<span class='label label-outline-info check_active1 open_cursor' data-status=".$row['availability_id']." data-incId=".$row['id']." data-tbname='products'>Out of Stock</span>" ;} ?></td>

            <!-- <td><?php echo $row['availability_id'];?></td> -->
            <td><?php if ($row['status']==0) { echo "<span class='label label-outline-success check_active open_cursor' data-incId=".$row['id']." data-status=".$row['status']." data-tbname='products
            '>Active</span>" ;} else { echo "<span class='label label-outline-info check_active open_cursor' data-status=".$row['status']." data-incId=".$row['id']." data-tbname='products'>In Active</span>" ;} ?></td>
            
            <td> <a href="edit_products.php?pid=<?php echo $row['id']; ?>"><i class="zmdi zmdi-edit"></i></a> &nbsp; <a href="product_details.php?pcode=<?php echo $row['product_code']; ?>"><i class="zmdi zmdi-eye zmdi-hc-fw" data-toggle="modal" data-target="#<?php echo $row['id']; ?>" class=""></i></a></td>
          </tr>
          <?php  $i++; } ?>
        </tbody>     
      </table>
    </div>
  </div>
</div>
   <?php include_once 'admin_includes/footer.php'; ?>
   <script src="js/tables-datatables.min.js"></script>