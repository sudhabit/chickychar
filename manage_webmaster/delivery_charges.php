<?php include_once 'admin_includes/main_header.php'; ?>

<?php  
 if (!isset($_POST['submit']))  {
      //If fail
        echo "fail";
    } else {
    //If success
    $id=1;
    
    $gst = $_POST['gst'];
    $delivery_charge = $_POST['delivery_charge'];
    $minimum_order = $_POST['minimum_order'];

    $minimum_order_placed_amnt = $_POST['minimum_order_placed_amnt'];
    $minimum_order_promo_amnt = $_POST['minimum_order_promo_amnt'];
    
      $sql = "UPDATE `delivery_charges` SET gst='$gst',delivery_charge = '$delivery_charge', minimum_order='$minimum_order' , minimum_order_placed_amnt='$minimum_order_placed_amnt', minimum_order_promo_amnt='$minimum_order_promo_amnt' WHERE id = '$id' ";
            if($conn->query($sql) === TRUE){
               echo "<script type='text/javascript'>window.location='delivery_charges.php?msg=success'</script>";
            } else {
               echo "<script type='text/javascript'>window.location='delivery_charges.php?msg=fail'</script>";
            }
            //echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
        
       
    
}
?>

      <div class="site-content">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="m-y-0">Delivery Charges</h3>
          </div>
          <div class="panel-body">            
            <div class="row">
              <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                <form data-toggle="validator" method="post" enctype="multipart/form-data">

                    <?php $getAllDeliveryCharges = getDataFromTables('delivery_charges',$status=NULL,$clause='id',$id=1,$activeStatus=NULL,$activeTop=NULL); 
                  $getDeliveryCharges = $getAllDeliveryCharges->fetch_assoc(); ?>
                    
                  
                  
                  <div class="form-group">
                    <label for="form-control-2" class="control-label">GST(%)</label>
                    <input type="text" name="gst" class="form-control" id="form-control-2" placeholder="GST(%)" data-error="Please enter GST(%)." value="<?php echo $getDeliveryCharges['gst'];?>" required>
                    <div class="help-block with-errors"></div>
                  </div>
                  <div class="form-group">
                    <label for="form-control-2" class="control-label">Delivery Charges</label>
                    <input type="text" name="delivery_charge" class="form-control" id="form-control-2" placeholder="Delivery Charges" data-error="Please enter Delivery Charges." value="<?php echo $getDeliveryCharges['delivery_charge'];?>" required>
                    <div class="help-block with-errors"></div>
                  </div>
                  <div class="form-group">
                    <label for="form-control-2" class="control-label">Add Minimum Amount For Delivery Charges </label>
                    <input type="text" name="minimum_order" class="form-control" id="form-control-2" placeholder="Minimum Order Charge" data-error="Please enter Minimum Order Charge." value="<?php echo $getDeliveryCharges['minimum_order'];?>" required>
                    <div class="help-block with-errors"></div>
                  </div>

                  <div class="form-group">
                    <label for="form-control-2" class="control-label">Add Minimum Amount For Order </label>
                    <input type="text" name="minimum_order_placed_amnt" class="form-control" id="form-control-2" placeholder="Add Minimum Amount For Order" data-error="Add Minimum Amount For Order" value="<?php echo $getDeliveryCharges['minimum_order_placed_amnt'];?>" required>
                    <div class="help-block with-errors"></div>
                  </div>

                  <div class="form-group">
                    <label for="form-control-2" class="control-label">Add Minimum Amount For Promo Code </label>
                    <input type="text" name="minimum_order_promo_amnt" class="form-control" id="form-control-2" placeholder="Add Minimum Amount For Promo Code" data-error="Add Minimum Amount For Promo Code." value="<?php echo $getDeliveryCharges['minimum_order_promo_amnt'];?>" required>
                    <div class="help-block with-errors"></div>
                  </div>

                  <button type="submit" name="submit" value="Submit" class="btn btn-primary btn-block">Submit</button>
                </form>
              </div>
            </div>
            <hr>           
          </div>
        </div>
      </div>
  
<?php include_once 'admin_includes/footer.php'; ?>