<?php include_once 'admin_includes/main_header.php'; ?>
<?php $getSubCategoriesData = getAllDataWithActiveRecent('product_type_details'); $i=1; ?>
     <div class="site-content">
        <div class="panel panel-default panel-table">
          <div class="panel-heading">
           <a href="add_product_type.php" style="float:right">Add Product Types</a>
            <h3 class="m-t-0 m-b-5">Product Types</h3>            
          </div>
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-striped table-bordered dataTable" id="table-1">
                <thead>
                  <tr>
                    <th>S.No</th>                    
                    <th>Product Type</th>
                    <th>Type Name</th>
                    <th>Type Price</th>
                    <th>Status</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  <?php while ($row = $getSubCategoriesData->fetch_assoc()) { ?>
                  <?php $product_type = getSingleColumnName($row['product_type_code'],'product_type_code','product_type','product_types'); ?>
                  <tr>
                    <td><?php echo $i;?></td>                    
                    <td><?php echo $product_type;?></td>
                    <td><?php echo $row['type_name'];?></td>
                    <td><?php if($row['type_price'] == "0") { echo '--'; } else { echo $row['type_price']; }?></td>
                    <td><?php if ($row['status']==0) { echo "<span class='label label-outline-success check_active open_cursor' data-incId=".$row['id']." data-status=".$row['status']." data-tbname='product_type_details'>Active</span>" ;} else { echo "<span class='label label-outline-info check_active open_cursor' data-status=".$row['status']." data-incId=".$row['id']." data-tbname='product_type_details'>In Active</span>" ;} ?></td>
                    <td> <a href="edit_product_type.php?product_type_code=<?php echo $row['product_type_code']; ?>&id=<?php echo $row['id']; ?>"> <i class="zmdi zmdi-edit"></i></a> </td>
                  </tr>
                  <?php  $i++; } ?>                  
                </tbody>               
              </table>
            </div>
          </div>
        </div>   
       </div>
   <?php include_once 'admin_includes/footer.php'; ?>
   <script src="js/tables-datatables.min.js"></script>