<?php include_once 'admin_includes/main_header.php'; ?>
<?php  if (!isset($_POST['submit']))  {
        echo "";
	} else  {
	    $rating = $_POST['rating'];   
      $product_id = $_POST['product_id'];
      $description = $_POST['description'];                                 
	    $status = $_POST['status'];     
      $created_at = date("Y-m-d h:i:s");                                               
	    $sql = "INSERT INTO product_review (`rating`,`product_id`,`description`,`status`,`created_at`) VALUES ('$rating','$product_id','$description','$status','$created_at')";
        if($conn->query($sql) === TRUE){
         echo "<script type='text/javascript'>window.location='products.php?msg=success'</script>";
        } else {
         echo "<script type='text/javascript'>window.location='products.php?msg=fail'</script>";
        }            
	 }
?>
<div class="site-content">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="m-y-0">Product Ratings</h3>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                <form data-toggle="validator" method="POST">
                  <div class="form-group">
                    <label for="form-control-2" class="control-label">Rating(Ex: 4.5)</label>
                    <input type="text" name="rating" class="form-control" id="user_input" placeholder="Please enter Product Rating" data-error="Please enter Product Rating" required >
                    <span id="input_status" style="color: red;"></span>
                    <div class="help-block with-errors"></div>                    
                    <input type="hidden" name="product_id" value="<?php echo $_GET['pid']; ?>">
                  </div>

                  <div class="form-group">
                    <label for="form-control-2" class="control-label">Description</label>
                    <input type="text" name="description" class="form-control" id="user_input" placeholder="Description" data-error="Description" required >
                    <span id="input_status" style="color: red;"></span>
                    <div class="help-block with-errors"></div>                    
                  </div>

                  <?php $getStatus = getDataFromTables('user_status',$status=NULL,$clause=NULL,$id=NULL,$activeStatus=NULL,$activeTop=NULL);?>
                  <div class="form-group">
                    <label for="form-control-3" class="control-label">Choose your status</label>
                    <select id="form-control-3" name="status" class="custom-select" data-error="This field is required." required>
                      <option value="">Select Status</option>
                      <?php while($row = $getStatus->fetch_assoc()) { ?>
                          <option value="<?php echo $row['id']; ?>"><?php echo $row['status']; ?></option>
                      <?php } ?>
                   </select>
                    <div class="help-block with-errors"></div>
                  </div>
                
                  <button type="submit" name="submit" class="btn btn-primary btn-block">Submit</button>
                </form>
              </div>
            </div>
            <hr>
          </div>
        </div>
      </div>
  
<?php include_once 'admin_includes/footer.php'; ?>