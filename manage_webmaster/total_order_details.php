<!DOCTYPE html>
<html lang="en">
  
<!-- Mirrored from big-bang-studio.com/cosmos/pages-invoice.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 28 Aug 2017 10:14:32 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="description" content="">
    <title>Cosmos</title>
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700" rel="stylesheet">
    <link rel="stylesheet" href="css/vendor.min.css">
    <link rel="stylesheet" href="css/cosmos.min.css">
    <link rel="stylesheet" href="css/application.min.css">
  </head>
  <body class="layout layout-header-fixed layout-left-sidebar-fixed">
    <div class="site-overlay"></div>
    
    <div class="site-main">
    
      

<?php include_once('admin_includes/config.php');
      include_once('admin_includes/common_functions.php');
?>
<?php $id = $_GET['order_id']; 

$getOrders = "SELECT * FROM orders WHERE order_id='$id'";
$getOrdersData = $conn->query($getOrders);
$getOrdersData1 = $getOrdersData->fetch_assoc();



?>
      <div class="site-content">
        <div class="panel panel-default m-b-0">
          <div class="panel-heading">
            <h3 class="m-y-0">Invoice</h3>
          </div>
          <div class="panel-body">
            <div class="row m-b-30">
              <div class="col-sm-6">
                <h4>Order Information</h4>               
                <p>Order Id: <?php echo $getOrdersData1['order_id']; ?></p>
                <p>Payment Method: <?php if($getOrdersData1['payment_type'] == '1' ) { echo "COD"; } else { echo "Online"; } ?></p>
                <?php $order_status = getIndividualDetails($getOrdersData1['order_status'],'order_status','id'); ?>
                <p>Order Status : <?php echo $order_status['status']; ?></p>
                <p>Payment Status: <?php if ($getOrdersData1['payment_status']==1) { echo "Pending" ;} elseif($getOrdersData1['payment_status']==2) {  echo "Completed"; } else { echo "Failed";}?></p>
              </div>
              <div class="col-sm-6">
                <h4>Shipping address</h4>
               <p><?php echo $getOrdersData1['first_name']; ?></p>
              <p><?php echo $getOrdersData1['email']; ?></p>
              <p><?php echo $getOrdersData1['mobile']; ?></p>
              <p><?php echo $getOrdersData1['address1']; ?></p>
              <p><?php echo $getOrdersData1['pin_code']; ?></p></td>

              </div>
            </div>
            <table class="table table-bordered m-b-30">
              <thead>
                <tr>
                  <th>Item Name</th>
                  <th>Quantity</th>
                  <th>Price</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
                <?php $getOrders1 = "SELECT * FROM orders WHERE order_id='$id'";
                      $getOrdersData3 = $conn->query($getOrders1);
                 while($getOrdersData2 = $getOrdersData3->fetch_assoc()) { 
                  ?>
                <tr>
                  <td><?php echo $getOrdersData2['product_name']; ?></td>
                  <td><?php echo $getOrdersData2['product_quantity']; ?></td>
                  <td><?php echo $getOrdersData2['product_price']; ?></td>
                  <td><?php echo $getOrdersData2['product_price']*$getOrdersData2['product_quantity']; ?></td>
                </tr>
                <?php } ?>
                <tr>
                  <td colspan="2">
                   
                    <!--<div class="text-right">
                      <?php if($getOrdersData1['delivery_charges'] != 0) { ?>
                      <br> Delivery Charges
                      <?php } ?>
                      <br>
                      <strong>TOTAL</strong>
                    </div>-->
                  </td>
                 
                   <td>
                    <p>Delivery Charges</p>
                    <p>GST:</p>                    
                    <?php if($getOrdersData1['coupon_code']!='') { ?>
                      <p style="color:green">Coupon Code (<?php echo $getOrdersData1['coupon_code']; ?>) </p>
                    <?php } ?>
                    <p><b>Total:</b></p></td>
                  <td>
                    <p><?php echo $getOrdersData1['delivery_charges']; ?></p>
                    <p><?php echo $getOrdersData1['gst']; ?></p>                   
                    <?php if($getOrdersData1['coupon_code']!='') { ?>
                      <p>-<?php echo $getOrdersData1['coupon_applied_amount']; ?></p>
                    <?php } ?>
                    <p><b><?php echo $getOrdersData1['order_total']; ?></b></p>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="panel-footer text-right">
            <button type="button" class="btn btn-primary btn-labeled" onclick="myFunction()">Print
              <span class="btn-label btn-label-right p-x-10">
                <i class="zmdi zmdi-print"></i>
              </span>
            </button>
          </div>
        </div>
      </div>
      <div class="site-footer">
        <?php include_once 'admin_includes/footer.php'; ?>
      </div>
    </div>
  </body>
<script>
function myFunction() {
   window.print();
}
</script>
<!-- Mirrored from big-bang-studio.com/cosmos/pages-invoice.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 28 Aug 2017 10:14:32 GMT -->
</html>