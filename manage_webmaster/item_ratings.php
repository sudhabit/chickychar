<?php include_once 'admin_includes/main_header.php'; ?>
<?php $getItemRatingsData = getAllDataWithActiveRecent('order_review'); $i=1; ?>
     <div class="site-content">
        <div class="panel panel-default panel-table">
          <div class="panel-heading">
            
            <h3 class="m-t-0 m-b-5">Reviews</h3>            
          </div>
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-striped table-bordered dataTable" id="table-1">
                <thead>
                  <tr>
                    <th>S.No</th>     
                    <th>Username</th>               
                    <th>Comments</th>
                    <th>Rating</th>                    
                    <th>Created</th>
                    <th>Status</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  <?php while ($row = $getItemRatingsData->fetch_assoc()) { ?>
                  <tr>
                    <td><?php echo $i;?></td>
                    <td><?php $getUname= getIndividualDetails($row['user_id'],'users','id'); echo $getUname['user_name'];?></td>        
                    <td><?php echo $row['description'];?></td>
                    <td><?php echo $row['rating'];?></td>                    
                    <td><?php echo $row['created_at'];?></td>
                    <td><?php if ($row['status']==0) { echo "<span class='label label-outline-success check_active open_cursor' data-incId=".$row['id']." data-status=".$row['status']." data-tbname='order_review'>Active</span>" ;} else { echo "<span class='label label-outline-info check_active open_cursor' data-status=".$row['status']." data-incId=".$row['id']." data-tbname='order_review'>In Active</span>" ;} ?></td>
                    <td><a href="delete_item_rating.php?bid=<?php echo $row['id']; ?>"><i class="zmdi zmdi-delete zmdi-hc-fw" onclick="return confirm('Are you sure you want to delete?')"></i></a></td>
                  </tr>
                  <?php  $i++; } ?>                  
                </tbody>               
              </table>
            </div>
          </div>
        </div>   
       </div>
   <?php include_once 'admin_includes/footer.php'; ?>
   <script src="js/tables-datatables.min.js"></script>