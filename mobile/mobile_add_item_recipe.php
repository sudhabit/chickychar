<?php 
error_reporting(0);
include "../manage_webmaster/admin_includes/config.php";
include "../manage_webmaster/admin_includes/common_functions.php";

if($_SERVER['REQUEST_METHOD']=='POST'){

	if (isset($_REQUEST['user_id']) && !empty($_REQUEST['category_name']) && isset($_REQUEST['ingredients']) && !empty($_REQUEST['how_to_prepare']) ) {
		// echo "<pre>"; print_r($_POST); die;	
		$user_id = $_REQUEST['user_id'];
		$category_id = $_REQUEST['category_name'];
		$ingredients = $_REQUEST['ingredients'];
		$how_to_prepare = $_REQUEST['how_to_prepare'];			
		$created_at = date("Y-m-d h:i:s");
		
		$sql = "INSERT INTO item_recipes (`category_id`, `ingredients`, `how_to_prepare`, `user_id`, `created_at`) VALUES ('$category_id','$ingredients','$how_to_prepare','$user_id','$created_at')";

		if ($conn->query($sql) === TRUE) {
            // check the conditions for query success or not
            $response["success"] = 0;            
            $response["message"] = "Save Successfully";               
        } else {
            // fail query insert problem
            $response["success"] = 2;
            $response["message"] = "Oops! An error occurred.";                      
        }

	}   else {
		//If post params empty return below error
		$response["success"] = 3;
	    $response["message"] = "Required field(s) is missing";	    
	}

} else {
	//Request invalid
	$response["success"] = 4;
	$response["message"] = "Invalid request";
}

echo json_encode($response);

?>