<?php
include "../manage_webmaster/admin_includes/config.php";
include "../manage_webmaster/admin_includes/common_functions.php";

$response = array();

if (isset($_REQUEST['cartId']) && !empty($_REQUEST['cartId'])  ) {
	
	$cartId = $_REQUEST['cartId'];
	$delCartItem = "DELETE FROM cart WHERE id = '$cartId' ";
	$conn->query($delCartItem);
	
    $response["success"] = 0;
    $response["message"] = "Cart Save Successfully";
         
} else {

    $response["success"] = 2;
    $response["message"] = "Required field(s) is missing";
}

echo json_encode($response);
?>